#!/usr/bin/env python3

"""Simple Go lang iota pattern implementation"""

# This file is a part of pyiota package
# Licensed under Do-What-The-Fuck-You-Want license
# Initially made by @jedi2light (aka Carey Minaieva)

from pyiota import iota

with iota.start():
  A = iota()
  B = iota()
  C = iota()

with iota.start(10):
  D = iota()
  E = iota()
  F = iota()

assert (0,1,2,10,11,12) == (A, B, C, D, E, F), 'Test failed'
